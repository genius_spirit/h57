import React, { Component } from 'react';
import MoneyManager from "./containers/MoneyManager";

class App extends Component {
  render() {
    return (
      <MoneyManager/>
    );
  }
}

export default App;
