import React from 'react';
import './FinanceInputPanel.css';

const FinanceInputPanel = ({text, amount, changeText, changeValue, click, select}) => {
  return(
    <div className="FinanceInputPanel">
      <input type="text" placeholder='Item name' className='inputPanel-text' value={text} onChange={changeText}/>
      <input type="text" className='inputPanel-value' value={amount} onChange={changeValue} />
      <span>som</span>
      <select className='inputPanel-select' onChange={select}>
        <option>Car</option>
        <option>Food</option>
        <option>Bills</option>
        <option>Health</option>
      </select>
      <button className='inputPanel-btn' onClick={click}>Add</button>
    </div>
  )
};

export default FinanceInputPanel;