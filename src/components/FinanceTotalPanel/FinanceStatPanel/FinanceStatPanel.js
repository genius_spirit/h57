import React, {Fragment} from 'react';
import './FinanceStatPanel.css';

const FinanceStatPanel = ({list, total}) => {

  let carCatAmount = 0;
  let foodCatAmount = 0;
  let billsCatAmount = 0;
  let healthCatAmount = 0;

  list.forEach(item => {
    switch (item.category) {
      case 'Car':
        return carCatAmount += parseInt(item.value, 10);
      case 'Food':
        return foodCatAmount += parseInt(item.value, 10);
      case 'Bills':
        return billsCatAmount += parseInt(item.value, 10);
      case 'Health':
        return healthCatAmount += parseInt(item.value, 10);
      default:
        return null;
    }
  });

  return(
    <Fragment>
      <div className="FinanceStatPanel-wrapper">
        <div className="FinanceStatPanel-car"
             style={total ? {width: `${carCatAmount / total * 100}%`} : null}></div>
        <div className="FinanceStatPanel-food"
             style={total ? {width: `${foodCatAmount / total * 100}%`} : null}></div>
        <div className="FinanceStatPanel-bills"
             style={total ? {width: `${billsCatAmount / total * 100}%`} : null}></div>
        <div className="FinanceStatPanel-health"
             style={total ? {width: `${healthCatAmount / total * 100}%`} : null}></div>
      </div>
      <div>
        <span className='item gold'></span>Car
        <span className='item green'></span>Food
        <span className='item maroon'></span>Bills
        <span className='item brown'></span>Health
      </div>
    </Fragment>

  )
};

export default FinanceStatPanel;