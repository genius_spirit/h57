import React from 'react';
import './FinanceTotalPanel.css';
import FinanceStatPanel from "./FinanceStatPanel/FinanceStatPanel";

const FinanceTotalPanel = ({total, list}) => {
  return(
    <div className='FinanceTotalPanel'>
      Total spent: {total} som
      <FinanceStatPanel list={list} total={total}/>
    </div>
  )
};

export default FinanceTotalPanel;