import React from 'react';
import './FinanceViewPanel.css';
import ViewPanelItem from "./ViewPanelItem/ViewPanelItem";

const FinanceViewPanel = ({list, remove}) => {

  return(
    <div className="FinanceViewPanel">
      {list.map((item) => {
        return <ViewPanelItem key={item.id} item={item} remove={() => remove(item.id)}/>
      })}
    </div>
  )
};

export default FinanceViewPanel;