import React from 'react';
import './ViewPanelItem.css';

const ViewPanelItem = ({item, remove}) => {
  return(
    <div className='ViewPanelItem'>
      <span className='ViewPanelItem-text'>{item.text}</span>
      <span className='ViewPanelItem-value'>{item.value}</span>
      <span className='ViewPanelItem-currency'>som</span>
      <span className='ViewPanelItem-category'>{item.category}</span>
      <input type="submit" value='X' className='ViewPanelItem-remove' onClick={remove}/>
    </div>
  )
};

export default ViewPanelItem;