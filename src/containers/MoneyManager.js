import React, { Component } from 'react';
import './MoneyManager.css';
import FinanceInputPanel from "../components/FinanceInputPanel/FinanceInputPanel";
import FinanceViewPanel from "../components/FinanceViewPanel/FinanceViewPanel";
import FinanceTotalPanel from "../components/FinanceTotalPanel/FinanceTotalPanel";

class MoneyManager extends Component {

  state = {
    list: [],
    currentText: '',
    currentValue: '',
    selectValue: '',
    totalAmount: 0,
  };

  idCount = 0;

  changeCurrentText = (event) => {
    let text = event.target.value;
    this.setState({currentText: text});
  };

  changeCurrentValue = (event) => {
    let amount = event.target.value;
    this.setState({currentValue: amount});
  };

  handlerSelect = (event) => {
    let select = event.target.value;
    this.setState({selectValue: select});
  };

  handlerAddItemToList = () => {
    if (this.state.currentText.length > 0 && !isNaN(parseInt(this.state.currentValue, 10))) {
      const listCopy = [...this.state.list];
      const newItem = {id: this.idCount, text: this.state.currentText, value: this.state.currentValue, category: this.state.selectValue || 'Car'};
      listCopy.push(newItem);
      let total = this.state.totalAmount;
      total += parseInt(this.state.currentValue, 10);
      this.idCount++;
      this.setState({list: listCopy, totalAmount: total, currentText: '', currentValue: ''});
    } else alert('!!!');
  };

  handlerRemoveItemFromList = (id) => {
    const index = this.state.list.findIndex(p => p.id === id);
    const listCopy = [...this.state.list];
    const itemCopy = {...listCopy[index]};
    listCopy.splice(index, 1);
    let total = this.state.totalAmount;
    total -= itemCopy.value;
    this.setState({list: listCopy, totalAmount: total});
  };

  render() {
    return(
      <div className='MoneyManager-container'>
        <FinanceInputPanel
          click={this.handlerAddItemToList}
          select={(event) => this.handlerSelect(event)}
          text={this.state.currentText}
          amount={this.state.currentValue}
          changeText={(event) => this.changeCurrentText(event)}
          changeValue={(event) => this.changeCurrentValue(event)}/>
        <FinanceViewPanel
          list={this.state.list}
          remove={this.handlerRemoveItemFromList}/>
        <FinanceTotalPanel total={this.state.totalAmount} list={this.state.list}/>
      </div>
    )
  }
}

export default MoneyManager;