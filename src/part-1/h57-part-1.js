const tasks = [
  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];


  let frontendTime = tasks.reduce((acc, item) => {
    item.category === 'Frontend' ? acc += item.timeSpent : null;
    return acc;
  }, 0);

  let bugsTime = tasks.reduce((acc, item) => {
    item.type === 'bug' ? acc += item.timeSpent : null;
    return acc;
  }, 0);

  let uiCount = tasks.reduce((acc, item) => {
    (item.title).includes('UI') ? acc += 1 : null;
    return acc;
  }, 0);

  const countCategories =  {Frontend: 0, Backend: 0};

  tasks.map(item => {
    item.category === "Frontend" ? countCategories.Frontend++ : countCategories.Backend++;
  });

  const array = [];

  tasks.map(item => {
    item.timeSpent > 4 ? array.push({title: item.title, category: item.category}) : null;
  });

  console.log(frontendTime, bugsTime, uiCount, countCategories, array);




